package order

import (
	"context"
	dbSQL "database/sql"
	"fmt"
	"strings"

	"example/model/dto"
	"example/model/mysql"
)

// Upd : 更新訂單 upd order
// Transaction 為選填
func Upd(ctx context.Context, tx *dbSQL.Tx, input *dto.Order) (err error) {
	if input == nil {
		return fmt.Errorf("參數錯誤")
	}

	var dbM *dbSQL.DB

	if tx == nil {
		dbM = mysql.GetConn("test")
	}

	sql := " UPDATE `order` SET "
	sql += "    `product_id` = ?"
	sql += " WHERE "

	var params []interface{}
	var wheres []string

	params = append(params, input.ProductID)

	// ID
	if input.ID != 0 {
		wheres = append(wheres, " `id` = ? ")
		params = append(params, input.ID)
	}

	// 沒有條件時回傳錯誤
	if len(wheres) == 0 {
		return fmt.Errorf("sql 語法錯誤")
	}

	sql += strings.Join(wheres, " AND ")

	// 執行sql

	if tx == nil {
		_, err = dbM.ExecContext(ctx, sql, params...)
	} else {
		_, err = tx.ExecContext(ctx, sql, params...)
	}
	if err != nil {
		return fmt.Errorf("upd錯誤 error: %v, sql: %v, params: %v ", err, sql, params)
	}

	return err
}
