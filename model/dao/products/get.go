package products

import (
	"context"
	dbSQL "database/sql"
	"fmt"
	"strings"

	"example/model/dto"
	"example/model/mysql"
)

// GetInput :
type GetInput struct {
	ID int64
}

// Get : 取得 user
func Get(ctx context.Context, input *GetInput, tx *dbSQL.Tx) (ret []*dto.Products, err error) {
	var dbS *dbSQL.DB

	if tx == nil {
		dbS = mysql.GetConn("test")
	}

	sql := " SELECT "
	sql += "    `id`,"
	sql += "    `name`,"
	sql += "    `price`"

	sql += " FROM `products` "
	sql += " WHERE "

	var params []interface{}
	var wheres []string

	// ID
	if input.ID != 0 {
		wheres = append(wheres, " `id` = ? ")
		params = append(params, input.ID)
	}

	// 沒有條件時回傳錯誤
	if len(wheres) == 0 {
		return nil, fmt.Errorf("sql 語法錯誤")
	}

	sql += strings.Join(wheres, " AND ")

	var rows *dbSQL.Rows

	if tx == nil {
		rows, err = dbS.QueryContext(ctx, sql, params...)
	} else {
		rows, err = tx.QueryContext(ctx, sql, params...)
	}

	if err != nil {
		return nil, fmt.Errorf("select 錯誤: %v", err)
	}

	defer rows.Close()

	for rows.Next() {
		data := &dto.Products{}
		if err := rows.Scan(
			&data.ID,
			&data.Name,
			&data.Price,
		); err != nil {
			return nil, fmt.Errorf("scan 錯誤: %v", err)
		}
		ret = append(ret, data)
	}

	return ret, err
}
