package user

import (
	"context"
	dbSQL "database/sql"
	"fmt"

	"example/model/dto"
	"example/model/mysql"
)

// Ins : 新增用戶 ins user
// Transaction 為選填
func Ins(ctx context.Context, tx *dbSQL.Tx, input *dto.User) (err error) {
	if input == nil {
		return fmt.Errorf("參數錯誤")
	}

	var dbM *dbSQL.DB

	if tx == nil {
		dbM = mysql.GetConn("test")
	}

	sql := " INSERT INTO users ("
	sql += "    id,"
	sql += "    account,"
	sql += "    password"
	sql += " )"
	sql += " VALUES "
	sql += " ( ?, ?, ?)"

	var params []interface{}

	params = append(params, input.ID)
	params = append(params, input.Account)
	params = append(params, input.Password)

	// 執行sql

	if tx == nil {
		_, err = dbM.ExecContext(ctx, sql, params...)
	} else {
		_, err = tx.ExecContext(ctx, sql, params...)
	}
	if err != nil {
		return fmt.Errorf("ins錯誤 error: %v, sql: %v, params: %v ", err, sql, params)
	}

	return err
}
