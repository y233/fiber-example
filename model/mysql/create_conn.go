package mysql

import (
	"database/sql"
	"fmt"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

var (
	// 資料庫連線物件
	pool map[string]*sql.DB

	// 同步鎖
	mu *sync.Mutex
)

func init() {
	pool = make(map[string]*sql.DB)
	mu = &sync.Mutex{}
}

// GetConn : 依照資料庫名稱取得DB連線
func GetConn(dbName string) *sql.DB {
	mu.Lock()
	defer mu.Unlock()

	if conn, ok := pool[dbName]; ok {
		if err := conn.Ping(); err == nil {
			return conn
		}
		conn.Close()
	}

	pool[dbName] = createConn(dbName)
	return pool[dbName]
}

// createConn : 建立資料庫連線
func createConn(dbName string) *sql.DB {
	host := "127.0.0.1"
	port := 3306
	user := "root"
	password := "root"

	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true", user, password, host, port, dbName)

	db, err := sql.Open("mysql", connectionString)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	return db
}
