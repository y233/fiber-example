package dto

import "time"

type Order struct {
	ID         int64     // 流水號
	UserID     string    // 會員ID
	ProductID  int64     // 商品ID
	CreateTime time.Time // 建立時間
	UpdateTime time.Time // 更新時間
}
