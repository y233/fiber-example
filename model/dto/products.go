package dto

type Products struct {
	ID    int64   // 流水號
	Name  string  // 名稱
	Price float64 // 金額
}
