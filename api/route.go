package api

import (
	"example/api/controller/back/member"
	"example/api/controller/back/order"
	"example/api/controller/back/product"
	"example/api/middleware"

	"github.com/gofiber/fiber/v2"
)

// SetupRoutes func
func SetupRoutes(app *fiber.App) {
	// 首頁
	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World 👋!")
	})

	// 後台 API - 未登入
	v1BackNonToken := app.Group("/v1/back")
	{
		v1BackNonToken.Post("/member/login", member.Login)   // 登入
		v1BackNonToken.Post("/member/create", member.Create) // 建立會員
		v1BackNonToken.Post("/member/info", member.Info)     // 會員資訊
		v1BackNonToken.Post("/product/info", product.Info)   // 商品資訊
	}

	// 後台 API - 已登入
	v1Back := app.Group("/v1/back", middleware.Token)
	{
		v1Back.Post("/member/update", member.Update)   // 更新會員
		v1Back.Post("/member/delete", member.Delete)   // 刪除會員
		v1Back.Post("/product/create", product.Create) // 新增商品
		v1Back.Post("/product/update", product.Update) // 更新商品
		v1Back.Post("/product/delete", product.Delete) // 刪除商品
		v1Back.Post("/order/create", order.Create)     // 新增訂單
		v1Back.Post("/order/info", order.Info)         // 訂單資訊
		v1Back.Post("/order/update", order.Update)     // 更新訂單
		v1Back.Post("/order/delete", order.Delete)     // 刪除訂單
	}
}
