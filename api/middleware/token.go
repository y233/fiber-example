package middleware

import (
	"fmt"

	"example/enum"
	"example/model/redis"

	"github.com/gofiber/fiber/v2"
)

func Token(c *fiber.Ctx) error {
	token := c.Get("api-token")

	if token == "" {
		return enum.NonLogin
	}

	uuid, err := redis.Get(c.Context(), fmt.Sprintf("token:%s", token))
	if err != nil {
		return ErrorHandler(err)
	}

	if uuid == "" {
		return enum.NonLogin
	}

	c.Set("userID", uuid)

	return c.Next()
}
