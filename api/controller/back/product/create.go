package product

import (
	"context"
	"time"

	"example/api/middleware"
	"example/api/protocol"
	"example/enum"
	"example/model/dao/products"
	"example/model/dto"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

type (
	CreateInput struct {
		Name  string  `json:"name" validate:"required,min=1,max=30"` // 名稱（長度： 1~30）
		Price float64 `json:"price" validate:"required"`             // 金額
	}

	createTask struct {
		APIName string
		Req     *CreateInput
		Res     *protocol.Response
		Storage *CreateStorage
	}

	// CreateStorage : 暫存
	CreateStorage struct {
		Err error
	}
)

// newCreateTask : 實例化Task
func newCreateTask() *createTask {
	return &createTask{
		APIName: "Create",
		Req:     &CreateInput{},
		Res: &protocol.Response{
			Code:    "1",
			Message: "OK",
			Result:  struct{}{},
		},
		Storage: &CreateStorage{},
	}
}

// Create : 新增
func Create(c *fiber.Ctx) error {
	task := newCreateTask()

	// 解析參數
	if shouldBreak := task.BindRequest(c); shouldBreak {
		return middleware.ErrorHandler(task.Storage.Err)
	}

	ctx, cancel := context.WithTimeout(c.Context(), time.Minute*15)
	defer cancel()

	// 新增商品
	if shouldBreak := task.DoCreate(ctx); shouldBreak {
		return middleware.ErrorHandler(task.Storage.Err)
	}

	return c.JSON(task.Res)
}

// BindRequest : 解析參數
func (task *createTask) BindRequest(c *fiber.Ctx) bool {
	if err := c.BodyParser(task.Req); err != nil {
		task.Storage.Err = enum.ParameterError(err)
		return true
	}

	err := validator.New().Struct(task.Req)
	if err != nil {
		task.Storage.Err = enum.ParameterError(err)
		return true
	}

	return false
}

// DoCreate : 新增商品
func (task *createTask) DoCreate(ctx context.Context) bool {
	err := products.Ins(
		ctx,
		nil,
		&dto.Products{
			Name:  task.Req.Name,
			Price: task.Req.Price,
		},
	)
	if err != nil {
		task.Storage.Err = err
		return true
	}

	return false
}
