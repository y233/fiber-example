package order

import (
	"context"
	"time"

	"example/api/middleware"
	"example/api/protocol"
	"example/enum"
	"example/model/dao/order"
	"example/model/dto"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

type (
	UpdateInput struct {
		ID        int64 `json:"id" validate:"required"`        // 訂單ID
		ProductID int64 `json:"productID" validate:"required"` // 商品ID
	}

	updateTask struct {
		APIName string
		Req     *UpdateInput
		Res     *protocol.Response
		Storage *UpdateStorage
	}

	// UpdateStorage : 暫存
	UpdateStorage struct {
		Err error
	}
)

// newUpdateTask : 實例化Task
func newUpdateTask() *updateTask {
	return &updateTask{
		APIName: "Update",
		Req:     &UpdateInput{},
		Res: &protocol.Response{
			Code:    "1",
			Message: "OK",
			Result:  struct{}{},
		},
		Storage: &UpdateStorage{},
	}
}

// Update : 更新
func Update(c *fiber.Ctx) error {
	task := newUpdateTask()

	// 解析參數
	if shouldBreak := task.BindRequest(c); shouldBreak {
		return middleware.ErrorHandler(task.Storage.Err)
	}

	ctx, cancel := context.WithTimeout(c.Context(), time.Minute*15)
	defer cancel()

	// 更新
	if shouldBreak := task.DoUpdate(ctx); shouldBreak {
		return middleware.ErrorHandler(task.Storage.Err)
	}

	return c.JSON(task.Res)
}

// BindRequest : 解析參數
func (task *updateTask) BindRequest(c *fiber.Ctx) bool {
	if err := c.BodyParser(task.Req); err != nil {
		task.Storage.Err = enum.ParameterError(err)
		return true
	}

	err := validator.New().Struct(task.Req)
	if err != nil {
		task.Storage.Err = enum.ParameterError(err)
		return true
	}

	return false
}

// DoUpdate : 更新
func (task *updateTask) DoUpdate(ctx context.Context) bool {
	// 查看訂單是否存在
	data, err := order.Get(
		ctx,
		&order.GetInput{
			ID: task.Req.ID,
		}, nil)
	if err != nil {
		task.Storage.Err = err
		return true
	}

	if len(data) == 0 {
		task.Storage.Err = enum.DataNotFound
		return true
	}

	// 更新訂單
	err = order.Upd(
		ctx,
		nil,
		&dto.Order{
			ID:        task.Req.ID,
			ProductID: task.Req.ProductID,
		})
	if err != nil {
		task.Storage.Err = err
		return true
	}

	return false
}
