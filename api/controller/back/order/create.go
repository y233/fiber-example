package order

import (
	"context"
	"time"

	"example/api/middleware"
	"example/api/protocol"
	"example/enum"
	"example/model/dao/order"
	"example/model/dto"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

type (
	CreateInput struct {
		UserID    string `json:"userID" validate:"required"`    // 會員ID
		ProductID int64  `json:"productID" validate:"required"` // 商品ID
	}

	createTask struct {
		APIName string
		Req     *CreateInput
		Res     *protocol.Response
		Storage *CreateStorage
	}

	// CreateStorage : 暫存
	CreateStorage struct {
		Err error
	}
)

// newCreateTask : 實例化Task
func newCreateTask() *createTask {
	return &createTask{
		APIName: "Create",
		Req:     &CreateInput{},
		Res: &protocol.Response{
			Code:    "1",
			Message: "OK",
			Result:  struct{}{},
		},
		Storage: &CreateStorage{},
	}
}

// Create : 新增
func Create(c *fiber.Ctx) error {
	task := newCreateTask()

	// 解析參數
	if shouldBreak := task.BindRequest(c); shouldBreak {
		return middleware.ErrorHandler(task.Storage.Err)
	}

	ctx, cancel := context.WithTimeout(c.Context(), time.Minute*15)
	defer cancel()

	// 新增訂單
	if shouldBreak := task.DoCreate(ctx); shouldBreak {
		return middleware.ErrorHandler(task.Storage.Err)
	}

	return c.JSON(task.Res)
}

// BindRequest : 解析參數
func (task *createTask) BindRequest(c *fiber.Ctx) bool {
	if err := c.BodyParser(task.Req); err != nil {
		task.Storage.Err = enum.ParameterError(err)
		return true
	}

	err := validator.New().Struct(task.Req)
	if err != nil {
		task.Storage.Err = enum.ParameterError(err)
		return true
	}

	return false
}

// DoCreate : 新增訂單
func (task *createTask) DoCreate(ctx context.Context) bool {
	err := order.Ins(
		ctx,
		nil,
		&dto.Order{
			UserID:    task.Req.UserID,
			ProductID: task.Req.ProductID,
		})
	if err != nil {
		task.Storage.Err = err
		return true
	}

	return false
}
