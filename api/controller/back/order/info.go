package order

import (
	"context"
	"time"

	"example/api/middleware"
	"example/api/protocol"
	"example/enum"
	"example/model/dao/order"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
)

type (
	InfoInput struct {
		ID int64 `json:"id" validate:"required"` // 訂單ID
	}

	InfoIOutput struct {
		ID         int64  `json:"id"`
		UserID     string `json:"userID"`
		ProductID  int64  `json:"productID"`
		CreateTime string `json:"createTime"`
		UpdateTime string `json:"updateTime"`
	}

	infoTask struct {
		APIName string
		Req     *InfoInput
		Res     *protocol.Response
		Storage *InfoStorage
	}

	// InfoStorage : 暫存
	InfoStorage struct {
		Err    error
		Output *InfoIOutput
	}
)

// newInfoTask : 實例化Task
func newInfoTask() *infoTask {
	return &infoTask{
		APIName: "Info",
		Req:     &InfoInput{},
		Res: &protocol.Response{
			Code:    "1",
			Message: "OK",
			Result:  struct{}{},
		},
		Storage: &InfoStorage{
			Output: &InfoIOutput{},
		},
	}
}

// Info :
func Info(c *fiber.Ctx) error {
	task := newInfoTask()

	// 解析參數
	if shouldBreak := task.BindRequest(c); shouldBreak {
		return middleware.ErrorHandler(task.Storage.Err)
	}

	ctx, cancel := context.WithTimeout(c.Context(), time.Minute*15)
	defer cancel()

	// 取得資料
	if shouldBreak := task.GetInfo(ctx); shouldBreak {
		return middleware.ErrorHandler(task.Storage.Err)
	}

	task.Res.Result = task.Storage.Output

	return c.JSON(task.Res)
}

// BindRequest : 解析參數
func (task *infoTask) BindRequest(c *fiber.Ctx) bool {
	if err := c.BodyParser(task.Req); err != nil {
		task.Storage.Err = enum.ParameterError(err)
		return true
	}

	err := validator.New().Struct(task.Req)
	if err != nil {
		task.Storage.Err = enum.ParameterError(err)
		return true
	}

	return false
}

// GetInfo : 取得資料
func (task *infoTask) GetInfo(ctx context.Context) bool {
	data, err := order.Get(
		ctx,
		&order.GetInput{
			ID: task.Req.ID,
		}, nil)
	if err != nil {
		task.Storage.Err = err
		return true
	}

	if len(data) == 0 {
		task.Storage.Err = enum.DataNotFound
		return true
	}

	task.Storage.Output.ID = data[0].ID
	task.Storage.Output.UserID = data[0].UserID
	task.Storage.Output.ProductID = data[0].ProductID
	task.Storage.Output.CreateTime = data[0].CreateTime.Format(time.RFC3339)
	task.Storage.Output.UpdateTime = data[0].UpdateTime.Format(time.RFC3339)

	return false
}
