package main

import (
	"fmt"
	"net/http"
	"strconv"

	"example/api"
	"example/api/protocol"
	"example/enum"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
)

func main() {
	app := fiber.New(fiber.Config{
		// Override default error handler
		ErrorHandler: func(ctx *fiber.Ctx, err error) error {
			// Return from handler
			if customError, ok := err.(*enum.CustomError); ok {
				return ctx.Status(customError.HTTPStatus).JSON(protocol.Response{
					Code:    strconv.Itoa(customError.Code),
					Message: customError.Message,
					Result:  struct{}{},
				})
			}

			return ctx.Status(http.StatusInternalServerError).JSON(protocol.Response{
				Code:    "99999",
				Message: fmt.Sprintf("例外錯誤 err:%v", err),
				Result:  struct{}{},
			})
		},
	})

	app.Use(logger.New())

	app.Use(recover.New(recover.Config{
		EnableStackTrace: true,
	}))

	api.SetupRoutes(app)

	app.Listen("127.0.0.1:3000")
}
